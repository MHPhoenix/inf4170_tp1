/**
 * Auteur: MONGBO Houefa Orphyse Peggy Merveille
 * 		   MONH08519906
**/

#include "mips.h"


int main (int argc, char *argv[]) {

	FILE * fichierDEntre = stdin;
	FILE * fichierDeSorti = stdout;
	char nomFichierDEntre [TAILLE];
	char nomFichierDeSortie [TAILLE];
	
	if (strlen (argv [1] ) != 0) {
			strcpy (nomFichierDEntre, argv [1]); //On copie le nom du fichier dans nomFichierDEntre
			strcpy (nomFichierDeSortie, nomFichierDEntre); //On copie le nom du fichier d'entre dans nomFichierDeSortie
			strcat (nomFichierDeSortie, ".asm"); //fichier de sortie avec extention .asm

		//On verifie que le nom du fichier existe et si oui si c'est la bonne extension
		if ( (valideExistenceFichier (argc) == false) || 
			 (valideExtensionFichier (nomFichierDEntre, TAILLE) == false) ) {
			printf ("Aucun fichier ou mauvaise extension\n");
			printf ("Relancez le programme avec un nom de fichier valide (Ex: nom.mips)\n");
			exit(0);
		}

		//Ouverture du fichier contenant les instructions
		fichierDEntre = fopen (nomFichierDEntre, "r");

		char instruction[TAILLE_MAX]; //lignes de chaque fichier

		//On s'assure que le fichier n'est pas vide
		if (fichierDEntre != NULL) {

			//On entre dans le dossier
			while (fgets(instruction, TAILLE_MAX, fichierDEntre) != NULL) {

				//Pour instructions avec parentheses
				for (int i = 0; instruction[i] != '\n'; i++) {

                	if (instruction[i] == '(') {
                    	instruction[i] = ' ';
					} else if (instruction[i] == ')') {
                    	instruction[i] = '\0';
					}
            	}

				char * separateurs = " ,"; //Definition des separateurs.
				// On recupere, un a un, tous les mots (token) de l'instruction.
    			char* instru = strtok (instruction, separateurs);
				int n = -1;
				char * morceau[4];

				//Recupere les lignes d'instructions et met chaque mot
				//(instruction et valeur) dans un tableau
				while (instru != NULL) {
					morceau[++n] = instru;
        			// On demande le mot (instruction suivante) suivant.
        			instru = strtok ( NULL, separateurs );
    			}

				bool estTypeR = false; //Permet de verifier si l'instruction est de type R
				int encodeur[6]; //Tableau d'encodage
				int opcode = 0; // 6bits
				int rs = 0; // 5bits
				int rt = 0; // 5bits
				int rd = 0; // 5bits
				int shant = 0; // 5bits
				unsigned int instructionBin;

				encodeur[1] = rs;
				encodeur[2] = rt;
				encodeur[3] = rd;
				encodeur[0] = opcode;
				encodeur[4] = shant;


				//Type R
				if (strcmp (morceau[0], add) == 0) {
					encodeur[5] = 32; //champ funct
					estTypeR = true;
				} else if (strcmp (morceau[0], addu) == 0) {
					encodeur[5] = 33;
					estTypeR = true;
				} else if (strcmp (morceau[0], and) == 0) {
					encodeur[5] = 36;
					estTypeR = true;
				} else if (strcmp (morceau[0], jr) == 0) {
					encodeur[5] = 8;
					estTypeR = true;
				} else if (strcmp (morceau[0], nor) == 0) {
					encodeur[5] = 39;
					estTypeR = true;
				} else if (strcmp (morceau[0], or) == 0) {
					encodeur[5] = 37;
					estTypeR = true;
				} else if (strcmp (morceau[0], sll) == 0) {	
					encodeur[5] = 0;
					estTypeR = true;
				} else if (strcmp (morceau[0], slt) == 0) {	
					encodeur[5] = 42;
					estTypeR = true;
				} else if (strcmp (morceau[0], sltu) == 0) {
					encodeur[5] = 43;
					estTypeR = true;
				} else if (strcmp (morceau[0], srl) == 0) {
					encodeur[5] = 2;
					estTypeR = true;
				} else if (strcmp (morceau[0], sub) == 0) {
					encodeur[5] = 34;
					estTypeR = true;
				} else if (strcmp (morceau[0], subu) == 0) {
					encodeur[5] = 35;
					estTypeR = true;
				} else if (strcmp (morceau[0], addi) == 0) { //Type I
					encodeur[0] = 8; //opcode
				} else if (strcmp (morceau[0], addiu) == 0) {
					encodeur[0] = 9;
				} else if (strcmp (morceau[0], andi) == 0) {
					encodeur[0] = 12;
				} else if (strcmp (morceau[0], beq) == 0) {
					encodeur[0] = 4;
				} else if (strcmp (morceau[0], bne) == 0) {
					encodeur[0] = 5;
				} else if (strcmp (morceau[0], lb) == 0) {
					encodeur[0] = 32;
				} else if (strcmp (morceau[0], lbu) == 0) {
					encodeur[0] = 36;
				} else if (strcmp (morceau[0], lh) == 0) {
					encodeur[0] = 33;
				} else if (strcmp (morceau[0], lhu) == 0) {
					encodeur[0] = 37;
				} else if (strcmp (morceau[0], lui) == 0) {
					encodeur[0] = 15;
				} else if (strcmp (morceau[0], lw) == 0) {
					encodeur[0] = 35;
				} else if (strcmp (morceau[0], ori) == 0) {
					encodeur[0] = 13;
				} else if (strcmp (morceau[0], sb) == 0) {
					encodeur[0] = 40;
				} else if (strcmp (morceau[0], sh) == 0) {
					encodeur[0] = 41;
				} else if (strcmp (morceau[0], slti) == 0) {
					encodeur[0] = 10;
				} else if (strcmp (morceau[0], sltiu) == 0) {
					encodeur[0] = 11;
				} else if (strcmp (morceau[0], sw) == 0) {
					encodeur[0] = 43;
				}

				//Comparaison des registres
				if (strcmp (morceau[1], zero) == 0) {
					rd = zeroN;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], at) == 0) {
					rd = atN;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], v0) == 0) {
					rd = v0N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], v1) == 0) {
					rd = v1N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], a0) == 0) {
					rd = a0N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], a1) == 0) {
					rd = a1N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], a2) == 0) {
					rd = a2N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], a3) == 0) {
					rd = a3N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t0) == 0) {
					rd = t0N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t1) == 0) {
					rd = t1N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t2) == 0) {
					rd = t2N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t3) == 0) {
					rd = t3N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t4) == 0) {
					rd = t4N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t5) == 0) {
					rd = t5N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t6) == 0) {
					rd = t6N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t7) == 0) {
					rd = t7N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s0) == 0) {
					rd = s0N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s1) == 0) {
					rd = s1N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s2) == 0) {
					rd = s2N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s3) == 0) {
					rd = s3N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s4) == 0) {
					rd = s4N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s5) == 0) {
					rd = s5N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s6) == 0) {
					rd = s6N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], s7) == 0) {
					rd = s7N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t8) == 0) {
					rd = t8N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], t9) == 0) {
					rd = t9N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], k0) == 0) {
					rd = k0N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], k1) == 0) {
					rd = k1N;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], gp) == 0) {
					rd = gpN;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], sp) == 0) {
					rd = spN;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], fp) == 0) {
					rd = fpN;
					encodeur[3] = rd;
				} else if (strcmp (morceau[1], ra) == 0) {
					rd = raN;
					encodeur[3] = rd;
				}

				if (strcmp (morceau[2], zero) == 0) {
					rs = zeroN;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], at) == 0) {
					rs = atN;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], v0) == 0) {
					rs = v0N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], v1) == 0) {
					rs = v1N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], a0) == 0) {
					rs = a0N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], a1) == 0) {
					rs = a1N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], a2) == 0) {
					rs = a2N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], a3) == 0) {
					rs = a3N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t0) == 0) {
					rs = t0N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t1) == 0) {
					rs = t1N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t2) == 0) {
					rs = t2N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t3) == 0) {
					rs = t3N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t4) == 0) {
					rs = t4N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t5) == 0) {
					rs = t5N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t6) == 0) {
					rs = t6N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t7) == 0) {
					rs = t7N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s0) == 0) {
					rs = s0N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s1) == 0) {
					rs = s1N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s2) == 0) {
					rs = s2N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s3) == 0) {
					rs = s3N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s4) == 0) {
					rs = s4N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s5) == 0) {
					rs = s5N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s6) == 0) {
					rs = s6N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], s7) == 0) {
					rs = s7N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t8) == 0) {
					rs = t8N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], t9) == 0) {
					rs = t9N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], k0) == 0) {
					rs = k0N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], k1) == 0) {
					rs = k1N;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], gp) == 0) {
					rs = gpN;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], sp) == 0) {
					rs = spN;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], fp) == 0) {
					rs = fpN;
					encodeur[1] = rs;
				} else if (strcmp (morceau[2], ra) == 0) {
					rs = raN;
					encodeur[1] = rs;
				}

				if (strcmp (morceau[3], zero) == 0) {
					rt = zeroN;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], at) == 0) {
					rt = atN;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], v0) == 0) {
					rt = v0N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], v1) == 0) {
					rt = v1N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], a0) == 0) {
					rt = a0N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], a1) == 0) {
					rt = a1N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], a2) == 0) {
					rt = a2N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], a3) == 0) {
					rt = a3N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t0) == 0) {
					rt = t0N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t1) == 0) {
					rt = t1N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t2) == 0) {
					rt = t2N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t3) == 0) {
					rt = t3N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t4) == 0) {
					rt = t4N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t5) == 0) {
					rt = t5N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t6) == 0) {
					rt = t6N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t7) == 0) {
					rt = t7N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s0) == 0) {
					rt = s0N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s1) == 0) {
					rt = s1N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s2) == 0) {
					rt = s2N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s3) == 0) {
					rt = s3N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s4) == 0) {
					rt = s4N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s5) == 0) {
					rt = s5N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s6) == 0) {
					rt = s6N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], s7) == 0) {
					rt = s7N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t8) == 0) {
					rt = t8N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], t9) == 0) {
					rt = t9N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], k0) == 0) {
					rt = k0N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], k1) == 0) {
					rt = k1N;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], gp) == 0) {
					rt = gpN;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], sp) == 0) {
					rt = spN;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], fp) == 0) {
					rt = fpN;
					encodeur[2] = rt;
				} else if (strcmp (morceau[3], ra) == 0) {
					rt = raN;
					encodeur[2] = rt;
				}

				
				//Ouverture du fichier dans lequel on va ecrire le code binaire
				fichierDeSorti = fopen(nomFichierDeSortie, "wb");

				if (estTypeR) { //Encodage de type R
					// bit le plus significatifs est a gauche
					instructionBin += (encodeur[0] << 26); //6bits
					instructionBin += (encodeur[1] << 21); //5bits
					instructionBin += (encodeur[2] << 16); //5bits
					instructionBin += (encodeur[3] << 11); //5bits
					instructionBin += (encodeur[4] << 6); //5bits
					instructionBin += (encodeur[5] << 0); //6bits

					//ecriture fichier binaire
					if (fichierDeSorti != NULL) {
        				fputs(instruction, fichierDeSorti);
    				}

				} else { //Encodage de type I

					int nombre = 0, nombre2 = 0; //pour les lmm
					nombre = atol(morceau[3]);
					nombre2 = atol(morceau[2]);
					if (nombre != 0) {
						encodeur[3] = nombre;
					} else if (nombre2 != 0) {
						encodeur[3] = nombre2 * encodeur[3];
					}

					// bit le plus significatifs est a gauche
					instructionBin += (encodeur[0] << 26); //6bits
					instructionBin += (encodeur[1] << 21); //5bits
					instructionBin += (encodeur[2] << 16); //5bits
					instructionBin += (encodeur[3] << 0); //16bits
					
					//ecriture fichier binaire
					if (fichierDeSorti != NULL) {
        				fputs(instruction, fichierDeSorti);
    				}

				}

        	}

		} else {
			printf("Erreur de lecture du fichier !!!");
		}

		fclose (fichierDEntre);
		fclose(fichierDeSorti);

		printf("SUCCES :D, le fichier .asm a bien ete cree\n");
	}

	return 0;

}


/**
* Verifie que le fichier existe
* Prends en paramettre le nombre d'arguments
**/
bool valideExistenceFichier (int argument) {

	bool existe = false;

	if (argument == 2) {
		existe = true;
	} else {
		existe = false;
	}

	return existe;
}


/**
* Verifie qu'il s'agit bien d'un fichier mips
* Prends en paramettre le nom du fichier et la taille
**/
bool valideExtensionFichier (char nomFichierDEntre[], int tailleDuNom) {

	bool bonneExtension = false;
	char *extension = strrchr(nomFichierDEntre, '.');

	if (strcmp(extension, ".mips") == 0) {
		bonneExtension = true;
	} else {
			bonneExtension = false;
    }

	return bonneExtension;
}
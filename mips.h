/**
 * Auteur: MONGBO Houefa Orphyse Peggy Merveille
 * 		   MONH08519906
**/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

//Taille du fichier de lecture
#define TAILLE_MAX 1000
//Taille du nom des fichiers d'entree et de sortie
#define TAILLE 200
#define N0MBRE_REGISTRE 32
#define TAILLE_NOM_REGISTRE 6
#define TAILLE_INSTRUCTION 40

bool valideExistenceFichier (int argument);
bool valideExtensionFichier (char nomFichierDEntre[], int tailleDuNom);

//32 registres MIPS
//const char TAB_REGISTRE [N0MBRE_REGISTRE][TAILLE_NOM_REGISTRE] = {
const char zero [] = "$zero";
const char at [] = "$at";
const char v0 [] = "$v0";
const char v1 [] = "$v1";
const char a0 [] = "$a0";
const char a1 [] = "$a1";
const char a2 [] = "$a2";
const char a3 [] = "$a3";
const char t0 [] = "$t0";
const char t1 [] = "$t1";
const char t2 [] = "$t2";
const char t3 [] = "$t3";
const char t4 [] = "$t4";
const char t5 [] = "$t5";
const char t6 [] = "$t6";
const char t7 [] = "$t7";
const char s0 [] = "$s0";
const char s1 [] = "$s1";
const char s2 [] = "$s2";
const char s3 [] = "$s3";
const char s4 [] = "$s4";
const char s5 [] = "$s5";
const char s6 [] = "$s6";
const char s7 [] = "$s7";
const char t8 [] = "$t8";
const char t9 [] = "$t9";
const char k0 [] = "$k0";
const char k1 [] = "$k1";
const char gp [] = "$gp";
const char sp [] = "$sp";
const char fp [] = "$fp";
const char ra [] = "$ra";

//32 registres MIPS numero
const int zeroN = 0;
const int atN = 1;
const int v0N = 2;
const int v1N = 3;
const int a0N = 4;
const int a1N = 5;
const int a2N = 6;
const int a3N = 7;
const int t0N = 8;
const int t1N = 9;
const int t2N = 10;
const int t3N = 11;
const int t4N = 12;
const int t5N = 13;
const int t6N = 14;
const int t7N = 15;
const int s0N = 16;
const int s1N = 17;
const int s2N = 18;
const int s3N = 19;
const int s4N = 20;
const int s5N = 21;
const int s6N = 22;
const int s7N = 23;
const int t8N = 24;
const int t9N = 25;
const int k0N = 26;
const int k1N = 27;
const int gpN = 28;
const int spN = 29;
const int fpN = 30;
const int raN = 31;


//Differentes instructions de types R
const char add [] = "add";
const char addu [] = "addu";
const char and [] = "and";
const char jr [] = "jr";
const char nor [] = "nor";
const char or [] = "or";
const char sll [] = "sll";
const char slt [] ="slt";
const char sltu [] ="sltu";
const char srl [] ="srl";
const char sub [] ="sub";
const char subu [] ="subu";

//Differentes instructions de types I
const char addi [] = "addi";
const char addiu [] = "addiu";
const char andi [] = "andi";
const char beq [] =	"beq";
const char bne [] =	"bne";
const char lb [] = "lb";
const char lbu [] =	"lbu";
const char lh [] = "lh";
const char lhu [] =	"lhu";
const char lui [] =	"lui";
const char lw [] = "lw";
const char ori [] =	"ori";
const char sb [] = "sb";
const char sh [] = "sh";
const char slti [] = "slti";
const char sltiu [] = "sltiu";
const char sw [] = "sw";
.phony: data clean


default: mips.o
	gcc -Wall -pedantic -std=c99  -o mips mips.o

mips.o: mips.c
	gcc -Wall -pedantic -std=c99  -c mips.c

test:
	./mips

clean:
	rm *.o
	rm mips

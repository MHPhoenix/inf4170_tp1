# TP1 INF4170 : architecture des ordinateurs Hiver 2020
# Professeur : Jaques BERGER

## Description

    ### Assembleur MIPS
    Vous devez rédiger un assembleur MIPS. Votre logiciel devra lire un fichier contenant des instructions MIPS et les encoder en binaire dans un fichier de sortie.

    Votre logiciel doit être rédigé en langage C et doit compiler (sans avertissement) sur un serveur de l'UQAM (détails à venir). Un makefile doit être fourni avec votre logiciel pour en effectuer la compilation complète. Le nom de votre exécutable doit être "mips".

    Votre logiciel doit pouvoir s'exécuter à la console. Il prendra en paramètre le nom du fichier à compiler. Le nom du fichier de sortie sera le nom du fichier d'entrée avec l'extension supplémentaire ".asm".

    Par exemple, la commande "mips ex1.mips" produira le fichier "ex1.mips.asm" contenant le binaire correspondant aux instructions du fichier d'entrée.

    Vous recevrez 4 exemples de fichiers d'entrée sur Moodle. Les instructions MIPS à supporter sont celles contenues dans ces 4 fichiers. Lors de la correction, votre logiciel sera exécuté avec ces fichiers d'entrée et d'autres fichiers composés des mêmes instructions mais avec des registres et valeurs immédiates différents.

    Vous ne devez pas faire de traitement particulier pour supporter le big-endian ou little-endian. Dans votre encodage, considérez que le bit le plus significatif est à gauche.

    ### Remise
    Le travail doit être fait seul. Le répertoire de travail contenant les fichiers doit être archivé dans un fichier zip et nommé selon le code permanent de l'auteur. L'archive doit être remise par Moodle avant la date indiqué dans Moodle. Aucun retard ne sera accepté et une pénalité sera appliquée pour une archive non conforme sans les codes permanents.

    Un makefile doit permettre de faire l'installation complète du logiciel à partir des sources.
        
    ### Pondération

        > Respect des exigences : 35%

        > Encodages : 50%

        > Qualité du code (indentation, nomenclature, absence de duplication de code) : 10%
        
        > Makefile : 5%

## Technologies

    langage C

## Auteur

Houefa Orphyse Peggy Merveille MONGBO (MONH08519906)

## Fonctionnement 

Peut etre execute en ligne de commande ou avec le Makefile fourni
    > -make : compilation
    > -make test : etablissement de liens et execution
    Assurez-vous de mettre le nom du fichier (fichier.mips) apres "./mips", au niveau de test pour que le code puisse prendre en compte le fichier.
    > -make clean : supprime les fichiers objets

## Contenu du projet

> **Le projet contient les fichiers suivants :**

    > -mips.h : contient toutes les librairies et variables globales ainsi que la definition des fonctions
    > -mips.c : contient le code source
    > -Makefile : pour la compilation (make : compilation, make test : executable, make clean : supprime les fichiers objets)
    > -README.md

## References

> **Documentation :**

    > - Notes de cours
    > - OpenClassRoom : https://openclassrooms.com/fr/courses/19980-apprenez-a-programmer-en-c
    > - koor.fr : https://koor.fr/C/cstring/strtok.wp
    > - https://www.commentcamarche.net/forum/affich-35608331-decouper-un-texte-avec-arduino

## Statut

Fonctionnel
Bien que le code ne soit pas optimal en raison de sa longueur